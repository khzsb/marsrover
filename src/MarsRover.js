function MarsRover(maxZonePoint, roverStartingPosition, commands) {

    self = this;
    this.compass = ['N', 'E', 'S', 'W'];
    this.allowedCommands = ['M', 'L', 'R'];
    this.RVCommands = commands.toUpperCase();
    
    //Starting Position vars
    this.rvStartPosition = roverStartingPosition.split(' ');
    this.rvStartX = this.rvStartPosition[0];
    this.rvStartY = this.rvStartPosition[1];  
    this.rvStartDirection = this.rvStartPosition[2];

    //current rover position vars
    this.rvPositionX, this.rvPositionY, this.rvDirection;

    //Max zone position vars
    this.maxZPoint = maxZonePoint.split(' ');
    this.maxPointX = this.maxZPoint[0];
    this.maxPointY = this.maxZPoint[1];



    //================================== Validations ==============================

    // Step 1 - Validate inputs of the Maximum point of the cartesian plane
    // i)  Are the inputs defined
    // ii) Are the inputs integers
    this.validZone = validateZone() ;

    // Step 2 - Validate inputs of the starting position point of the rover in the cartesian plane
    // i)   Are the inputs defined
    // ii)  Are the inputs integers and a valid direction symbol
    // iii) Are the inputs within the cartesian plane
    this.validStartPos = validateStartPosition();

    // Step 3 - Validate inputs of the commands supplied
    this.validCommands = validateCommands(commands);

    
    function validateZone() {     //Validate the given Maximum Point of the cartesain plane. In this case it's x = 10, y = 8... Making it 80 square in the zone    
    //Step 1

                    var pointx = false, pointy = false, processcode = false;
                    var retResponse = new Array();
                    
                    // i) Are the inputs defined
                    processcode = (self.maxPointX === undefined || self.maxPointY === undefined) ? false : true; //check if maximum point has values 
                    
                    if(processcode){ //if maximum point is defined 
                        
                    //ii) Are the inputs integers
                                    pointx = isInputInteger(self.maxPointX); // Check if x value is integer
                                    pointy = isInputInteger(self.maxPointY); // Check if y value is integer
                                    (pointx === true && pointy === true) ? retResponse = [200, true, 'cartesian plane zone / maximum point is valid'] : retResponse = [400, false, 'cartesan plane zone / maximum point is invalid, please check your input e.g 2 1']  ;    
                                    
                                    
                                    
                    
                                }else{
                                    return retResponse = [500, false, 'One or both of the maximum (x or y) cartesian coordinates is undefined'];
                                }
                    return retResponse;
    }

    
    
    function validateStartPosition() {     //Validate the given Maximum Point of the cartesain plane. In this case it's x = 10, y = 8... Making it 80 square in the zone    
    //Step 2
                        var pointx = false, pointy = false, roverFace = false, processcode = false; 
                        var retResponse = new Array(); //[Error Number (200 / 500), Validity (false/true), error message]

                        // i) Are the inputs defined
                        processcode = (self.rvStartX === undefined || self.rvStartY === undefined || self.rvStartDirection === undefined) ? false : true; //check if Rover Starting position has values
                        
                        if(processcode){ //if starting point is defined
                            
                        // ii)  Are the inputs integers and a valid direction symbol
                                            pointx = isInputInteger(self.rvStartX); // Check if x value is integer
                                            pointy = isInputInteger(self.rvStartY); // Check if x value is integer                               
                                            (roverDirection(self.rvStartDirection)) ? roverFace = true : roverFace; // Check if rover direction input value is N/E/W/S
                                            
                        // iii) Are the inputs within the cartesian plane                    
                                            (pointx === true && pointy === true && roverFace === true) ? retResponse = isStartPointWithinZone() : retResponse = [400, false, 'Rover starting position is invalid, please check your input e.g 2 10 N'] ;    
                                            (retResponse[0]===200) ? setCurrentRoverPosition(self.rvStartX, self.rvStartY, self.rvStartDirection) : '';

                        }else{

                            return retResponse = [400, false, 'One or more of the rover\'s starting position points are missing or undefined. Expected example of inputs are as follows: 1 2 N'];
                        }
                        

                        return retResponse;
    }

    

    
    
    function validateCommands(commands) {     //Validate the given Maximum Point of the cartesain plane. In this case it's x = 10, y = 8... Making it 80 square in the zone    
    //Step 3 
                        var comms = false, processcode = false; 
                        var retResponse = new Array(); //[Error Number (200 / 500), Validity (false/true), error message]

                        processcode = (commands === undefined) ? false : true; //check if maxZonePoint has values
                        
                        if(processcode){
                            comms = roverCommands(commands);
                            (comms) ? retResponse = [200, true, 'Supplied commands are valid'] : retResponse = [400, false, 'Supplied commands are invalid, check if only M, L or R are in your commands'] ;
                        }
                        else
                        {
                            retResponse = [500, false, 'Supplied commands are undefined'];
                        }
                        
                        return retResponse;
        
    }

    //=============================================================================================






    //Console.dir only used to test outputs here. Remove them when using this for production.
    //console.dir('Zone: ' + this.validZone[0]);
    //console.dir('Starting Position: ' + this.validStartPos)
    //console.dir('Commands: ' + this.validCommands[0]);

    if(this.validZone[0] === 200)
    {

        if(this.validStartPos[0] === 200){

            if(this.validCommands[0] === 200){

                console.dir("Final Rover Position: "+moveTheRover(self.RVCommands));

            }else{
                console.error(this.validCommands[2]);
            }

        }else{
            console.error(this.validStartPos[2]);
        }

    }
    else{
        console.error(this.validZone[2]);
    }
    

    function setCurrentRoverPosition(x, y, z) //sets the current position of the rover: x - horizontal positio, y - vertical position, z - direction facing
    {
        self.rvPositionX = x;
        self.rvPositionY = y;
        self.rvDirection = z;
    }


    function isStartPointWithinZone(){ //Checks if the inputed starting position of the rover are greater than (0, 0) and less than the maximum point in the zone e.g (8 10)
        var retResponse = new Array();
        ((parseInt(self.rvStartX) >= 0 && parseInt(self.rvStartX) <= parseInt(self.maxPointX)) && (parseInt(self.rvStartY) >= 0 && parseInt(self.rvStartY) <= parseInt(self.maxPointY))) ? retResponse = [200, true, 'Rover is within zone bounds'] : retResponse = [400, false, 'Rover is out of zone bounds, please check coordinates'];
        return retResponse;
    }


    function isInputInteger(input_) //Checks if a value supplied is of Integer type, returns true or false
    {
        var confirmInput = false;
        (Number.isInteger(parseInt(input_))) ? confirmInput = true : confirmInput ;
        return confirmInput;
    }



    function moveTheRover(commands) {
    
        // Recieves a command, checks if its 'M' - Move rover, 'L' - turn rover Left, 'R'- turn rover Right
            for(var d = 0; d < commands.length; d++) { //Loop through the number of commands
                
                var command = commands[d];

                if (command === 'M') {
                    move();
                } else if (command === 'L' || command === 'R') {
                    turn(command);
                }
                console.log("(" +self.rvPositionX + ", " + self.rvPositionY + ", " + self.rvDirection + ")"); //shows you step by step position of the rover, you can comment this part when in production
            }

            var finalRoverPos = new Array();
            finalRoverPos = [self.rvPositionX, self.rvPositionY, self.rvDirection];
            return finalRoverPos; //returns final position of the rover
        
    }


    function roverDirection(rFace) { //Checks if rover direction supplied is N, W, E or S
      
            if (self.compass.includes(rFace)) return true;
            else return false;
        
    }

    function roverCommands(commz){ //Checks if the commands are either M, L or R
        for(var d = 0; d < commz.length; d++) {
            if (self.RVCommands.includes(commz[d])) return true;
            else return false;
        }
    }


    function move() { //Moving the rover forward/backwards through X axis or Y axis of the cartesian plan


        if (self.rvDirection === 'N') {

            self.rvPositionY++;

          } else if (self.rvDirection === 'S') {

            self.rvPositionY--;

          } else if (self.rvDirection === 'W') {

            self.rvPositionX--;

          } else if (self.rvDirection === 'E') {

            self.rvPositionX++;
            
          } else {
            console.error('Rover is now out of zone bounds');
          }

         

    }

    function turn(command) { // Turning the direction of the rover to face E, S, W or N depending on command


        if (self.rvDirection === 'N') {

            self.rvDirection = command === 'L' ? 'W' : 'E';

          } else if (self.rvDirection === 'S') {

            self.rvDirection = command === 'L' ? 'E' : 'W';

          } else if (self.rvDirection === 'W') {

            self.rvDirection = command === 'L' ? 'S' : 'N';
            
          } else if (self.rvDirection === 'E') {

            self.rvDirection = command === 'L' ? 'N' : 'S';

          }

    }


}