# MarsRover

This project is based on a Mars challenge where you  are supposed to navigate rovers in a zone carefully surveyed ahead of time. 
You are given the maximum point in the zone, the starting point and direction its facing.

**The specification of the project:**


*  Mars’s surface can be thought of as a zone that is itself a two-dimensional grid of
square areas.

*  The zones have been very carefully surveyed ahead of time and are deemed safe for
exploration within the landing terrain bounds, as represented by a single cartesian
coordinate - for example: (5, 5).

*  The rover understands the cardinal points and can face either East (E), West (W), North
(N) or South (S) at any given time.

*  The rover understands three commands:


    *  M - Move one space forward in the direction it is facing
    *  R - rotate 90 degrees to the right
    *  L - rotate 90 degrees to the left
    

*  Due to the transmission delay in communicating with the rover on Mars, you are only
able to send the rover a single list of commands.

*  These commands will be executed by the rover and its resulting location sent back to
HQ. This is an example of the list of commands sent to the rover:

8 10
1 2 E
MMLMRMMRRMML





**Solution:**


This is how we will call the main function:

 - MarsRover('8 10', '1 2 E', 'MMLMRMMRRMML')


The first we will have to do is validate all the inputs as follows:

'8 10' - Maximum point in the zone:
    - Check if they are defined correctly and integer


'1 2 E' - Starting position in the cartesian plane (1 2) and E represent direction rover is facing
    - Check if they are defined correctly, are integers (1 2)
    - Check if the direction is correct (N / E / W / S)
    - Check if the starting position falls within the zone
    
'MMLMRMMRRMML' - Commands the rover is supposed to execute:
    - Make sure commands are upper cased
    - Check if they are defined
    - Check if they are either 'M' for move, 'L' for turn Left or 'R' for turn Right
    


Returned responses will be structured as follows:

[Code, Flag, Message]

Code: 200 - means all is well, 400 - missing information or wrong inputs supplied, 500 - undefined inputs
Flag: true - means all is okay, false - means failed
Message: Description of what went wrong or confirms that executed correctly



If all validations pass, then the function to move the rover is executed as follows:

By looping through the commands, the following will be checked:

If the command is 'M':
    
*  If current facing direction is 'N'
        - Increase rover y axis position by 1
    
*  If current facing direction is 'E'
        - Increase rover x axis position by 1    
    
*  If current facing direction is 'S'
        - Decrease rover y axis position by 1
    
*  If current facing direction is 'W'
        - Decrease rover x axis position by 1
        
If the command is 'L':
    
*  If current facing direction is 'N'
        - Change direction to 'W'
    
*  If current facing direction is 'E'
        - Change direction to 'N'   
    
*  If current facing direction is 'S'
        - Change direction to 'E'
    
*  If current facing direction is 'W'
        - Change direction to 'S'
    
        
If the command is 'R':
    
*  If current facing direction is 'N'
        - Change direction to 'E'
    
*  If current facing direction is 'E'
        - Change direction to 'S'   
    
*  If current facing direction is 'S'
        - Change direction to 'W'
    
*  If current facing direction is 'W'
        - Change direction to 'N'        
        
        
        
        